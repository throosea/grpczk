module throosea.com/grpczk

go 1.16

require (
	github.com/samuel/go-zookeeper v0.0.0-20201211165307-7117e9ea2414
	google.golang.org/grpc v1.26.0
)
